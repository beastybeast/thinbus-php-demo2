<?php

// This header makes sure that inline javascript is not possible
header("Content-Security-Policy: default-src 'self'; frame-src 'self'; style-src 'self' 'unsafe-inline';");

// Make sure that the demo can not run in a fram from another site
header("X-Frame-Options: DENY");

// Set text as UTF-8 to make national characters display in the correct way
header('Content-type: text/html; charset=utf-8');  

?>

<!-- The included SRP files below are for demo and testing purposes -->
<script type='text/javascript' src='client/lib/biginteger.js'></script>
<script type='text/javascript' src='client/lib/isaac.js'></script>
<script type='text/javascript' src='client/lib/random.js'></script>
<script type='text/javascript' src='client/lib/sha256.js'></script>
<script type='text/javascript' src='client/lib/thinbus-srp6client.js'></script>
<script type='text/javascript' src='client/lib/thinbus-srp-config.js'></script> <!-- Recommended to replace large safe prime in this file -->
<script type='text/javascript' src='client/lib/thinbus-srp6a-config-sha256.js'></script>

<!-- The included SRP files below are minimized versions to be used in the real world
<script type='text/javascript' src='client/lib/thinbus-srp6a-sha256-versioned.js'></script>
-->

<!-- These files are specific for the demo -->
<script type='text/javascript' src='client/client.js'></script>
<script type='text/javascript' src='demo.js'></script>
