<?php

require_once("demo.inc.php");

?>

<!DOCTYPE html>
<html>
  <head>
    <link href='demo.css' rel='stylesheet' type='text/css' media='all'>
    <meta content="text/html; charset=windows-1252" http-equiv="content-type">
    <title>Thinbus SRP6a JS/PHP demo2</title>
  </head>
  <body>
    <h2>SRP Demo2</h2>
    <p>
    This is a simple demo of <a target="_blank" href="https://en.wikipedia.org/wiki/Secure_Remote_Password_protocol">SRP6a</a>
	using the <a target="_blank" href="https://bitbucket.org/simon_massey/thinbus-php">Thinbus PHP
	library</a> with Javascript as the client and PHP as the logical server
	code. I have created this demo as I think all examples that I have seen is
	too complicated to follow and/or understand.
	</p>
	<p>
	You will need to place all files in a folder on a PHP enabled server
	accessible from the web deamon/browser (please maintain the folder
	structure). Start up your favorite browser and direct it to the folder
	location. The system have been tested with PHP 5.3.3 on Linux. The demo
	need a folder with write access for storing information about registred users (for example /tmp)
	</p>  
	<p>
	The demo is very simpel. Just enter e-mail address and password in
	the input fields below. Activate 'Register' to create an account on the
	server. Activate 'Login' to try and login with the protocol. In case of
	success you will be redirected to another page!
	</p>
    <br>
    <hr style="width: 100%; color: black;">
    <table style="width: 100%;" border="0">
      <tbody>
        <tr>
          <td style="width: 100px; text-align: right;">E-mail</td>
          <td style="height: 19px;">
          <input id="email" name="email" type="email">
          </td>
        </tr>
        <tr>
          <td style="text-align: right;">Password</td>
          <td>
          <input id="pass" name="pass" type="password">
          </td>
        </tr>
        <tr>
          <td style="text-align: right;"></td>
          <td>
          <input value="Register" id="register" name="register" type="button">&nbsp;<input value="Login" id="login" name="login" type="button">
          </td>
        </tr>
      </tbody>
    </table>
    <hr style="width: 100%; color: black;">
    <br>
    <a id="displaysourcecode" href='#'>Show me how to use the SRP lib code</a>
    <div id="sourcecodediv" style="display:none;">
    <h2>Source code</h2>    
    <p>
    When creating this demo I had the following goals:
    <ul>    
    <li>Clean code to understand with a good layout</li>
    <li>Avoid use of libraries and source code beside the Thinbus code</li>
    <li>Use best practise everywhere</li>
    <li>Avoid mixing up HTML/JS/PHP code in the same file</li>
    </ul>
    </p>
    <p>
    The top folder contains the files that you need to understand and modify to make 
    your own system that implement SRP6a on the client side. If you need
    more explanation about the math and implementation details about the 
    lib please visit the <a target="_blank" href="https://bitbucket.org/simon_massey/thinbus-srp-js/overview">Thinbus main site</a> 
    </p> 
    <p> 
    The communication source code is separated in 2 main sub folders. One for the client logic
    and one for server logic. In the client folder the file client.js contains code 
    for calls to the file server.php located in the server folder. In each sub folder
    there is a lib folder with the core Thinbus SRP source files (do not change).
    </p>
    <p>
    The demo use ajax calls for communication between client and server. It is not a requirement in any 
    way so it is also possible to use post or other ways of communicating when using SRP. 
    The demo enable 
    <a target="_blank" href="https://en.wikipedia.org/wiki/Content_Security_Policy">Content-Security-Policy</a>
    to prevent cross-site scripting (XSS) attacks and other related attacks.
    This makes it complicated to have one or more static javascript files to handle page events as all 
    events from all pages get mixed together. 
    Because of this the demo dynamically inject or add a js file to each logical php/html file with the same name
    as the php file (only index.js and success.js in the demo).
    </p>
    <p>
    The demo use the <a target="_blank" href="http://www.redbeanphp.com/">ReadBeanPHP</a>
    logic with a text file database to store info about registered users. Source files for
    this is located as a db sub folder under the server folder. You may
    want to replace the text database with another database like MySQL or
    similar. To do this you will need to modify the db.php file. 
    You can also replace all database logic by modifying server.php file at relevant places.
    </p>
    <p>
    Below I specify how to use the library - but please also read the source files
    </p>
    <h3>Register</h3>
    <p>
    To register a user from the client side you need to create a SRP client
    object with this call:
    <pre class="codeexample">var srpclient = new SRP6JavascriptClientSessionSHA256();</pre>
    Then you need to generate a random salt value:
    <pre class="codeexample">var srpsalt = srpclient.generateRandomSalt();</pre>
    Then you need to generate a verifier value:
    <pre class="codeexample">var srpverifier = srpclient.generateVerifier(salt, email, password);</pre>
    Then send all the needed information to the server (email, salt, verifier)
    and store it somewhere for lather verification of the login procedure.
    The demo use a ajax function called jsonrpc in client.js that transfer data to the 
    server.php file as a json string. The reply is also a json string. 
    <br>
    <br>
    <a id="registercode" href='#'>See function from source code</a>
    <pre><code><div id="registerdiv" class="codesource"></div></code></pre>  
    </p>    
    <h3>Login</h3>
    <p>
    First step in the login procedure is to create a SRP client object again
    with this call:
    <pre class="codeexample">var srpclient = new SRP6JavascriptClientSessionSHA256();</pre>
    Initialize the client object with email and password:
    <pre class="codeexample">srpclient.step1(email, password);</pre>
    Send email to server as identifier and get data.salt, data.b from
    server. Use the data response to generate credentials:
    <pre class="codeexample">var credentials = srpclient.step2(data.salt, data.b);</pre>
    Send email, credentials.A, credentials.M1 to server and get data.M2 from 
    server. Use the data response to verify if user know the
    correct password:
    <pre class="codeexample">if(srpclient.step3(data.M2))</pre>
    Now get the shared session key (server has the same)
    <pre class="codeexample">var sessionkey = srpclient.getSessionKey();</pre>
    Please make sure to delete the srpclient object or re-direct the browser to
    a new page as the last step in the login procedure!
    <br>
    <br>
    Not complicated - I hope you agree!
    <br>
    <br>
    <a id="logincode" href='#'>See function from source code</a>
    <pre><code><div id="logindiv" class="codesource"></div></code></pre> 
    </p> 
    <h3>Prime used</h3>
    <p>    
    It is recommended to replace the default large safe prime values used (numeric constants) with your own.
    This can be done if you have access to a linux terminal and the openssl lib is installed. 
    Just follow the guide at this page: <a target="_blank" href="https://thinbus-n00p.rhcloud.com/dhparam">Generate prime constants for thinbus</a>
    </p>
    <p>  
    The file thinbus-srp-config.php contains the SRP PHP constants which looks something like:
    <pre class="codeexample">
$SRP6CryptoParams = [
    "N_base10" => "19502997308..."
    "g_base10" => "2",
    "k_base16" => "1a3d1769e1d..."
    "H" => "sha256"
];</pre>
    The numeric constants must match the values configured in the JavaScript file thinbus-srp-config.js.
    </p>
    </div>
    <br>
    <br>
    <a id="displaylicense" href='#'>Show me the license</a>
    <div id="licensediv" style="display:none;">
    <h2>License</h2>   
    <pre>
Copyright 2015 Benny Nissen

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    <a target="_blank" href="http://www.apache.org/licenses/LICENSE-2.0">http://www.apache.org/licenses/LICENSE-2.0</a>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.</pre>
    </div>    
    <h2>Contact</h2
    <p>
    If you need to contact me please use the contact form on the <a target="_blank"
      href="https://pcrypt.org/contact.php">Password Crypt</a> site where I have also used the Thinbus
    library for a real login system.
    </p>
    <p>
    Hope that you with this demo can understand how to use SRP6a for your own
    needs!
    </p>
    <p>
    Benny Nissen
    <br>
    2015  - version 0.1
    </p>
  </body>
</html>
