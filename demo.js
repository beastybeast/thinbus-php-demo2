/* 
This file contain some simple helper functions for the demo
*/

// Server to communicate with for this demo (you may need to change it to your own server location)
var ajaxurl = window.location.protocol + '//' + window.location.host + removefilefrompath(window.location.pathname) + '/server/server.php';

/*
The code below ensure that a coresponding js file is injected/loaded for each PHP DOM file
(unique js file for each php file)
*/
var jsfile = location.pathname.substring(location.pathname.lastIndexOf('/')+1);
jsfile = jsfile.substr(0, jsfile.lastIndexOf('.')) || 'index';
jsfile = jsfile + '.js';

/*
Add the file when the DOM content is loaded
*/
document.addEventListener("DOMContentLoaded", loadjscssfile(jsfile, 'js'), false);

/*
Inject JS or CSS file in DOM function
*/
function loadjscssfile(filename, filetype)
{
  var fileref;
  
  if (filetype == "js")
  {
    fileref = document.createElement('script');
    fileref.setAttribute("type","text/javascript");
    fileref.setAttribute("src", filename);
  }
  else if (filetype=="css")
  {
    fileref = document.createElement("link");
    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", filename);
  }
  
  if (typeof fileref == "undefined")
    return false;

  return document.getElementsByTagName("head")[0].appendChild(fileref); 
}

/*
Used to show or hide an element in the DOM (index file)
*/
function togglediv(elementId)
{
	var ele = document.getElementById(elementId);
	
	if(ele.style.display == "block") 
  {
    ele.style.display = "none";
  }
	else 
	{
		ele.style.display = "block";
	}
}

/*
Simple test function for valid email adr.
*/
function validemail(email) 
{
	var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
	
	return emailPattern.test(email); 
} 

/*
Remove file part from a full file path with folders
*/
function removefilefrompath(pathname)
{
    if(pathname[pathname.length-1] == '/')
      return pathname.substring(0, pathname.length - 1);

    var pathArray = pathname.split( '/' );    
    var newPathname = "";
    
    for( i = 0 ; i < (pathArray.length - 1) ; i++ ) // get path without filename
    if(pathArray[i].length)
    {
      newPathname += "/";
      newPathname += pathArray[i];
    }
    
    return newPathname;
}

