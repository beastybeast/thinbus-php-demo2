/*
This file is the logical javascript code to index.php and is dynamically loaded 
by demo.js

// DOMContentLoaded does not work with Chrome because this script is injected
document.addEventListener("DOMContentLoaded", function(event) {});

// Jquery ready work just fine
$(document).ready(function(){}); 
*/

// We need to initialize some things after the page is ready
window.onload = function (event) 
{ 
  document.getElementById('register').onclick = function (event)
  {
    srpregister(event);
  };
  
  document.getElementById('login').onclick = function (event)
  {
    srplogin(event);
  };
  
  document.getElementById('displaysourcecode').onclick = function (event)
  {
    togglediv('sourcecodediv');
    return false; // avoid that the page move to the top
  };
  
  document.getElementById('registerdiv').innerHTML = srpregister.toString();
  
  document.getElementById('registercode').onclick = function (event)
  {
    togglediv('registerdiv');
    return false; // avoid that the page move to the top
  };
  
  document.getElementById('logindiv').innerHTML = srplogin.toString();
  
  document.getElementById('logincode').onclick = function (event)
  {
    togglediv('logindiv');
    return false; // avoid that the page move to the top
  };
  
  document.getElementById('displaylicense').onclick = function (event)
  {
    togglediv('licensediv');
    return false; // avoid that the page move to the top
  };
};

/*
Get email and password from input fields
*/
function getinput()
{
  var emailfield = document.getElementById('email');
    
  if(!validemail(emailfield.value))
  {
    alert('Please enter a valid e-mail adr.');
    emailfield.focus();
    return false; 
  }
    
  var passfield = document.getElementById('pass');
    
  if(passfield.value.length == 0)
  {
    alert('Please enter a password');
    passfield.focus();
    return false;
  }
    
  return {email: emailfield.value, pass: passfield.value};  
}

/*
The actual register logic for the client
*/
function srpregister(event)
{
  var input = getinput(); // Get email and password from input fields
  
  if(input == false) // test if input is valid
    return;
    
  document.body.style.cursor  = 'wait';
  
  var srpclient = new SRP6JavascriptClientSessionSHA256();
  var srpsalt = srpclient.generateRandomSalt();
  var srpverifier = srpclient.generateVerifier(srpsalt, input.email, input.pass);
  
  // Send the data to the server with an ajax call
  jsonrpc(ajaxurl, 'register', {email: input.email, srpsalt: srpsalt, srpverifier: srpverifier}, 0, function (data, error, id)
  {
    document.body.style.cursor  = 'default';
    
    if(error)
    {
      alert(data);
      return;  
    }
    
    alert('You have with success registered user: ' + input.email);    
  });
}

/*
The actual login logic for the client

Please consider to use use some sort of Key Derivation Function on the password like PBKDF2, bcrypt or scrypt
This to avoid simple brute force attacks on the login procedure
*/
function srplogin(event)
{
  var input = getinput(); // Get email and password from input fields
  
  if(input == false) // test if input is valid
    return;
    
  document.body.style.cursor  = 'wait';
  
  var srpclient = new SRP6JavascriptClientSessionSHA256();
  
  srpclient.step1(input.email, input.pass);  
  
  // Send the data to the server with an ajax call (first step)
  jsonrpc(ajaxurl, 'login1', {email: input.email}, 0, function (data, error, id)
  {
    if(error)
    {
      document.body.style.cursor  = 'default';      
      alert(data);
      return;  
    }
    
    var credentials = srpclient.step2(data.salt, data.b);
    
    jsonrpc(ajaxurl, 'login2', {email: input.email, A: credentials.A, M1: credentials.M1}, 0, function (data, error, id)
    {
      document.body.style.cursor  = 'default';
      
      if(error)
      {
        alert(data);
        return;  
      }
      
      if(!srpclient.step3(data.M2))
      {
	      alert('Wrong password specified');
	      return;
      }
      
      // Store the session in session storage (session is now the same for both server and client and it has never been transmitted)
      sessionStorage['sessionkey'] = srpclient.getSessionKey();
      
      // Redirect the browser to the success page (this also clear all values)
      window.location.assign(window.location.protocol + '//' + window.location.host + removefilefrompath(window.location.pathname) + '/success.php'); 
      
    });
  });
}



