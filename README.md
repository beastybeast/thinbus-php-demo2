# Thinbus SRP PHP Demo2

Copyright (c) Benny Nissen, 2015

Demo of Secure Remote Password (SRP-6a) protocol implementation of a browser authenticating to a PHP server using the [Thinbus](https://bitbucket.org/simon_massey/thinbus-srp-js) Javascript library.

I have created this demo as I think all examples that I have seen is too complicated to follow and/or understand.

You will need to place all source files in a folder on a PHP enabled server accessible from the web (maintain the folder structure). 
Please launch the file index.php in a browser to test and read all details about the Demo2 implementation. The source code have been tested with PHP 5.3.3 on Linux. 

A live demo can be seen here: [http://pcrypt.dk/dev/rod/demo/](http://pcrypt.dk/dev/rod/demo/)

## License

```
   Copyright 2015 Benny Nissen

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```
   
End.