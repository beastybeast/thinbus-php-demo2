<?php
 /**
 * @author      ruslan.zavackiy and Benny Nissen
 * @since       X.X
 * @version     $Id$
 */

error_reporting(E_ALL);

if(version_compare(phpversion(), '5.3.4', '<')) 
  require 'rb-p533.php';
else
  require 'rb.php'; 

/*
Test if a specific folder have write permissions
*/
if(!function_exists('dir_write_access')) 
{
    function dir_write_access($dir) 
    {      
      $temp = tempnam($dir, '');
      
      if($temp == false)
        return false;
      
      $ourFileHandle = fopen($temp, 'w');
      
      if(!$ourFileHandle) 
        return false;
        
      fclose($ourFileHandle);
      unlink($temp);
      
      return true;
    }
}

/*
Search for a folder with write access
*/
if(!function_exists('dir_get_tmp')) 
{
    function dir_get_tmp() 
    {       
        // Standard test
        $temp = ini_get('upload_tmp_dir') ? ini_get('upload_tmp_dir') : sys_get_temp_dir();
        
        if(dir_write_access($temp))
          return $temp;
         
        // check environment variables.
        foreach (array('TMP', 'TEMP', 'TMPDIR') as $env_var) 
        {
            if ($temp = getenv($env_var)) 
            {
                if(dir_write_access($temp))
                  return $temp;
            }
        }
        
        // try current script folder        
        $temp = dirname(__FILE__);
        
        if(dir_write_access($temp))
          return $temp;
        
        // last try is current working folder (may be the same as script folder)
        $temp = getcwd();
        
        if(dir_write_access($temp))
          return $temp;
        
        // couldn't find a temp directory.
        return false;
    }
}

/*
Initialize the database connection
*/
if(!function_exists('startdatabase')) 
{
    function startdatabase() 
    { 
      $tmpfolder = dir_get_tmp();

      if(!$tmpfolder)
      {
        trigger_error("SRPDEMO: Unable to get write access");
        return false;
      }

      R::setup('sqlite:' . $tmpfolder . '/srp_db.txt');
      
      /*
        // if you like to connect to a mysql db replace the above code with the line below (see more here: http://www.redbeanphp.com/connection)      
        R::setup( 'mysql:host=localhost;dbname=mydatabase', 'user', 'password' ); //for both mysql or mariaDB
      */
      return true;
    }
}

