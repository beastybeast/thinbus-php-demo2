<?php

/*
This example can be enhanced in many ways to improve performance and 
protect against possible errors.
For example there is no protection against brute force attacks.
The database may get filled up with records from sessions etc
But this has been discarded to make it a more clean example of SRP
*/

require_once("db/db.php");

require_once("lib/BigInteger.php");
require_once("lib/srand.php");
require_once("lib/thinbus-srp-config.php");
require_once("lib/thinbus-srp.php");

header("Content-type: application/json; charset=utf-8");

try
{
  $jsondata = json_decode($HTTP_RAW_POST_DATA);
  
  $method = $jsondata->method;
  $data = $jsondata->data;
  $id = $jsondata->id;
}
catch(Exception $e)
{
  die(makereply($id, 'Unable to decode json data: ' . $e->getMessage(), 1));
}

if(!startdatabase())
  die(makereply($id, 'Unable to get write access for database', 1));
  
/*
Find out what we need to do
*/
switch($method)
{
	default:
		die(makereply($id, 'Unknown method specified', 1));
	break;
	
	case 'register':
		srpregister($data, $id);
	break;
	
	case 'login1':
		srplogin1($data, $id);
	break;
	
	case 'login2':
		srplogin2($data, $id);
	break;
	
	case 'logout':
		srplogout($data, $id);
	break;
}

/*
This function format a reply to the client the right way
*/
function makereply($id, $result, $error)
{	
	$replyjson = array();

	$replyjson['result'] = $result;
	$replyjson['error'] = $error;
	$replyjson['id'] = $id;

	return json_encode($replyjson);
}

/*
The actual function to create a user from a registration request
*/
function srpregister($data, $id)
{
  $user = R::findOne('user', 'email = :email', array(':email' => $data->email));

  if(!empty($user)) 
    die(makereply($id, 'User already exists: ' . $data->email, 1));
    
  $user = R::dispense('user');
  $user->email = $data->email;
  $user->srpsalt = $data->srpsalt;
  $user->srpverifier = $data->srpverifier;
  $dbid = R::store($user);

  die(makereply($id, true, null));
}

/*
First login step
*/
function srplogin1($data, $id)
{
	global $SRP6CryptoParams;

	$user = R::findOne('user', 'email = :email', array(':email' => $data->email));

	if(empty($user)) 
		die(makereply($id, 'User does not exists: ' . $data->email, 1));
		
	 $srp = new ThinbusSrp($SRP6CryptoParams["N_base10"], $SRP6CryptoParams["g_base10"], $SRP6CryptoParams["k_base16"], $SRP6CryptoParams["H"]);
	 
	 $resultarray = array();
	 $resultarray['salt'] = $user->srpsalt;
	 $resultarray['b'] = $srp->step1($data->email, $user->srpsalt, $user->srpverifier);
	 
	 // you might consider to make session a memory table because of speed issues
	 $session = R::findOne('session', 'email = :email', array(':email' => $data->email));
	 
	 if(empty($session)) 
		$session = R::dispense('session');
		 
	 $session->email = $data->email;
	 $session->srp = serialize($srp);	 
	 $dbid = R::store($session);
	 
	 die(makereply($id, $resultarray, null)); 
}

/*
Second login step
*/
function srplogin2($data, $id)
{
	$session = R::findOne('session', 'email = :email', array(':email' => $data->email));

	if(empty($session)) 
		die(makereply($id, 'User does not exists: ' . $data->email, 1));
    
	$srp = unserialize($session->srp);
    
    try 
    {
        $M2 = $srp->step2($data->A, $data->M1);

        $session->srp = $srp->getSessionKey(); // overwrite the old data        
        $dbid = R::store($session);
        
        $resultarray = array();
		    $resultarray['M2'] = $M2;
		    $resultarray['UserId'] = $srp->getUserID();
		    
		    if($data->email != $resultarray['UserId'])
          die(makereply($id, 'User id is not the same', 1));
        
    } 
    catch (Exception $e) 
    {
        die(makereply($id, 'Authentication failed: ' . $e->getMessage(), 1));
    }
    
     die(makereply($id, $resultarray, null)); 
}

/*
Logout logic - also demonstrate how to send the session variable to validate the request
You can also just set it as a PHP session variable as needed by your needs
*/
function srplogout($data, $id)
{
	$session = R::findOne('session', 'srp = :srp', array(':srp' => $data->session));
	
	if(empty($session)) 
		die(makereply($id, 'Unable to find session', 1)); // you may want to hide this as a safety feature
		
	R::trash($session);	
	
	die(makereply($id, true, null)); 
}

?>
