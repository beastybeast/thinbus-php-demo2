/*
Send json formattet data with ajax
*/
function jsonrpc(url, method, data, id, callback)
{
	var tmpObj = {};
	
	tmpObj.method = method;
	tmpObj.data = data;
	tmpObj.id = id;

	jsoncom('POST', url, JSON.stringify(tmpObj), function(http)
	{
		if(http.status == 200)
		{
			try 
			{
				 var tmpObjreply = JSON.parse(http.responseText);
			}
			catch(e)
			{
				if(callback)
					callback('Exception in parsing: ' + e.message, 'jsonrpc', null);
					
			  return;
			}
			
			if(callback)
				callback(tmpObjreply.result, tmpObjreply.error, tmpObjreply.id);
		}
		else
		{
			if(callback)
			{
        if(typeof(http) == 'object')
          callback('HTTP returned status: ' + http.status, 'http', null);
        else if(typeof(http) == 'string')
          callback('HTTP returned string status: ' + http, 'http', null);
        else
          callback('HTTP returned unknown error,', 'http', null);
      }
		}
	});
}

/*
Low level ajax procedure (the actual call)
*/
function jsoncom(method, url, data, dofunc)
{
  try 
	{
    var http = new XMLHttpRequest(); 

    http.open(method, url, true); 

    //Send the proper header information along with the request
    http.setRequestHeader("Content-type", "application/json");	

    http.onreadystatechange = function() // Call a function when the state changes.
    {
      if(dofunc) 
      if(http.readyState == 4)
      {			
        dofunc(http);
      }
    }
    http.send(data);
  }
  catch(e)
  {
    dofunc(e.message);
  }
}
