/*
This file is the logical javascript code to success.php and is dynamically loaded 
by demo.js

// DOMContentLoaded does not work with Chrome because this script is injected
document.addEventListener("DOMContentLoaded", function(event) {});

// Jquery ready work just fine
$(document).ready(function(){}); 
*/

// We need to initialize some things after the page is ready
window.onload = function (event) 
{ 
  document.getElementById('logout').onclick = function (event)
  {
    srplogout(event);
  };
}

/*
The actual logout logic
*/
function srplogout(event)
{
	document.body.style.cursor  = 'wait';
	
	jsonrpc(ajaxurl, 'logout', {session: sessionStorage['sessionkey']}, 0, function (data, error, id)
    {
      document.body.style.cursor  = 'default';
      
      if(error)
      {
        alert(data);
        return;  
      }
      
      // Redirect the browser back to the login page
      window.location.assign(window.location.protocol + '//' + window.location.host + removefilefrompath(window.location.pathname) + '/index.php');       
    });
 }
